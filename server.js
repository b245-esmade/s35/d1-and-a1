// allow express.js
const express = require('express');

// allow mongoose
const mongoose = require('mongoose');

// set local host port number
const port = 3001;

// create variable that uses and stores it as app
const app = express();

// [SECTION] MongoDB connection
	// Syntax:
		/*

		mongoose.connect('mongoDBconnectionString',{
			option to avoid errors in our connection
		})
		
		*/

	mongoose.connect('mongodb+srv://admin:admin@batch245-esmade.c4tetod.mongodb.net/s35-discussion?retryWrites=true&w=majority',
		{
			// to avoid error/impact to future changes while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true
		})


	let db = mongoose.connection;

	// error handling in connecting 
	db.on('error', console.error.bind(console,'Connection error'));

	// trigger success message if connection is successful
	db.once('open',()=> console.log('We are connected to the cloud database!'))

	// Mongoose Schemas
		// Schemas determine the structure of the documents to be written in the DB
		// Acts as blueprint to our data
		
		/*Syntax

			const schemaName = new mongoose.Schema({keyvaluepairs})
		*/

	// Start Task Schema
	// taskSchema contains 2 properties: name & status
	// required is used to specify that a field must not be empty
	// default - is used if a field value is not supplied

	const taskSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Task name is required!"]
		},
		status: {
			type: String,
			default: "pending"
		}
	})

	// End Task Schema

	// Start User Schema
	// userSchema contains 2 properties: username & password
	const userSchema = new mongoose.Schema({
		username: {
			type: String,
			required: [true, "username is required!"]
		},
		password: {
			type: String,
			default: "********"
		}
	})

	// End User Schema


	// [SECTION] Models
		// uses schema to create/instantiate documents/objects that follows our schema structure

		// the variable/object that will be create can be use to run commands with our database

		/*Syntax

			const vraiableName = mongoose.model("collectionName",schemaName);
		*/

	// task model
	const Task = mongoose.model("Task", taskSchema )

	// user model
	const User = mongoose.model("User", userSchema )



//Set up Middlewares

	app.use(express.json()) //allows to read json data
	app.use(express.urlencoded({extended: true})) //allows app to read data from forms



// [SECTIONS] Routing
	// Create/Add new task
		/*
		1.Check if the task is existing.
			//if already exists in DB, we will return a message "The task is already existing!"
			//if do not exist in the database, we will add it in the database

		*/


	// Start Task Routing
	app.post("/tasks",(request, response) => {
		let input = request.body

		console.log(input.status);
		if (input.status === undefined){

				Task.findOne({name:input.name},(error, result) => {

				console.log(result);

				if (result !== null){
					return response.send("The task is already existing")
				}
				else{
					let newTask = new Task({
						name: input.name
					})

					// save() method will save the object in the collection that the object instantiated
						newTask.save((saveError, savedTask) => {
							if (saveError){
							return console.log(saveError);
						}
						else {
							return response.send("New Task has been created")
						}
					})
				}
			})


		} else{
			Task.findOne({name:input.name},(error, result) => {

			console.log(result);

			if (result !== null){
				return response.send("The task is already existing")
			}
			else{
				let newTask = new Task({
					name: input.name,
					status: request.body.status
				})

				// save() method will save the object in the collection that the object instantiated
					newTask.save((saveError, savedTask) => {
						if (saveError){
						return console.log(saveError);
					}
					else {
						return response.send("New Task has been created")
					}
				})
			}
		})

		}


	})

	// End Task Routing


	// Start User Routing

	app.post("/signup",(request, response) => {
		let input = request.body

		User.findOne({username:input.username},(error, result) => {

			console.log(result);

			if (result !== null){
				return response.send("This user already exists ")
			}
			else{
				let newUser = new User({
					username: input.username
				})

					newUser.save((saveError, savedUser) => {
						if (saveError){
						return console.log(saveError);
					}
					else {
						return response.send("This user has been added")
					}
				})
			}
		})
	})




	// End User Routing

	// [SECTION] Retrieving all the data/tasks

	app.get('/tasks',(request, response) => {
		Task.find({},(error, result) => {
			if (error){
				console.log(error);
			}
			else {
				return response.send(result);
			}
		})
	})


	app.get('/signup',(request, response) => {
		User.find({},(error, result) => {
			if (error){
				console.log(error);
			}
			else {
				return response.send(result);
			}
		})
	})





	app.listen(port, ()=> console.log(`Server is running at port ${port}`))